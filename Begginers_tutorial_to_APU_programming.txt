Getting started tutorial to APU programming:

Example 1 - bit-wise AND between vectors of 16 bits
    Host topics:
        None
    Device topics:
        GVML Library:
            * gvml_vr16 abstraction
            * gvml_init_once
            * gvml_and_16
            * gvml_cpy_imm_16
            * gvml_get_entry_16
        GAL Library:
            * Performance monitor:
            * gal_pm_start
            * gal_get_pm_inst_count
            * gal_pm_stop
        APL code:
            * Multi-bit read APL
            * bit-serial APL

Example 2- bit-wise XNOR between vectors of 16 bits
    Host topics:
        * GDL library:
            * gdl_init and gdl_exit
            * gdl_context_alloc and gdl_context_free
            * gdl_run_task_timeout
    Device topics:
        GVML Library:
            * gvml_xor_16
            * gvml_not_16
        Utilities:
            * gsi_info 
        APL code:
            * _gvml_xnor

Example 3- Intro to I/O 1
    Host topics:
        * Common struct for APU and host.
        * Allocate and free data on device memory (gdl_mem_alloc and gdl_mem_free).
        * Use memory handles (gdl_mem_handle_to_host_ptr).
        * Direct access to device memory using pointers.
    Device topics:
        * Direct access: ARC to L4 (gal_mem_handle_to_apu_ptr).
        * Loading data from L4 to MMB using L2DMA (my_dma_l4_to_l1).
        * Storing data from MMB to L4 using L2DMA (my_dma_l1_to_l4).

Example 4- Intro to I/O 2
    Host topics:
        * Coping data to/from APU using GDL library (gdl_mem_cpy_to_dev and gdl_mem_cpy_from_dev).
        * Accessing APU output.
    Device topics:
        * Loading data from L4 to MMB using asynchronous functions:
            * my_dma_l4_to_l2_16k
            * gal_fast_l2dma_async_memcpy_end
            * my_dma_l2_to_l1_32k
        * Storing data from MMB to L4 using asynchronous functions:
            * my_dma_l1_to_l2_32k
            * my_dma_l2_to_l4_16k

Example 5- End-to-end MAX(A,B) application
    Find the element-wise max of 2 vectors.
    Device topics:
        * Using GVML markers
        * gvml_gt_u16
        * gvml_cpy_16_mrk
        * gvml_not_m
