----------------------------------------------------------------------------
 README for building and running an example-gvml_app for simulation and hardware
-----------------------------------------------------------------------------
---
## Required GSI internal version of MESON  
pip3 install meson -i http://192.168.42.9:8081/repository/gsi-pypi/simple --trusted-host 192.168.42.9
New from almog:
pip3 install meson -i http://cancun-vm:8081/repository/gsi-pypi/simple --trusted-host cancun-vm
## Must be connected to GSI VPN in order to install MESON
## Very recommended to use any python virtual enviroment like conda
## Be sure that Synopsis toolchain is setup !!!
---
## Setup options
1. first time requared clone all subprojects by : copy .wrap files to subprojects folder. then: meson subprojects download
2. Create meson compilation directory:
    2.1 Simulator Debug: meson setup ./<build_sim_debug>/
    2.2 Simulator Release: meson setup ./<build_sim_release>/ --buildtype release
    2.3 HW Debug: meson setup ./<build_hw_debug>/ --cross-file ./subprojects/gsi-common-meson/cross-files/archs36_l4.txt
    2.4 HW Release: meson setup ./<build_hw_release>/ --cross-file ./subprojects/gsi-common-meson/cross-files/archs36_l4.txt --buildtype release
---
## Compilation
meson compile -C <Required_version_folder>
---
## Run
Binary placed in <Required_version_folder>
./<Required_version_folder>/<app_name>
