/*
 * Copyright (C) 2021, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#ifndef APL_DEFS_H
#define APL_DEFS_H

// #ifdef __APL_PREPROC__

/* rownum registers used for function parameters */
#define RN_REG_G0 RN_REG_0
#define RN_REG_G1 RN_REG_1
#define RN_REG_G2 RN_REG_2
#define RN_REG_G3 RN_REG_3
#define RN_REG_G4 RN_REG_4
#define RN_REG_G5 RN_REG_5
#define RN_REG_G6 RN_REG_6
#define RN_REG_G7 RN_REG_7

/* rownum registers to hold predefined temporary vector-registers.
 * These rownum registers shouldn't change during program execution.
 */
#define RN_REG_T0 RN_REG_8
#define RN_REG_T1 RN_REG_9
#define RN_REG_T2 RN_REG_10
#define RN_REG_T3 RN_REG_11
#define RN_REG_T4 RN_REG_12
#define RN_REG_T5 RN_REG_13
#define RN_REG_T6 RN_REG_14

/* rownum register to hold pre-defined flags vector-registers.
 * This rownum register shouldn't change during program execution.
 */
#define RN_REG_FLAGS RN_REG_15


/* l1_addr registers used for function parameters */
#define L1_ADDR_REG0 L1_ADDR_REG_0
#define L1_ADDR_REG1 L1_ADDR_REG_1
#define L1_ADDR_REG2 L1_ADDR_REG_2
#define L1_ADDR_REG3 L1_ADDR_REG_3

#define L2_ADDR_REG0 L2_ADDR_REG_0

#define RE_REG_G0 RE_REG_0
#define RE_REG_G1 RE_REG_1
#define RE_REG_T0 RE_REG_2
#define RE_REG_NO_RE RE_REG_3

#define EWE_REG_G0 EWE_REG_0
#define EWE_REG_G1 EWE_REG_1
#define EWE_REG_T0 EWE_REG_2
#define EWE_REG_NO_EWE EWE_REG_3


/* general purpose smaps registers */
#define SM_REG0 SM_REG_0
#define SM_REG1 SM_REG_1
#define SM_REG2 SM_REG_2
#define SM_REG3 SM_REG_3

/* smaps registers to hold predefined section maps.
 * These smaps registers shouldn't change during program execution.
 */
#define SM_0XFFFF SM_REG_4
#define SM_0X0001 SM_REG_5
#define SM_0X1111 SM_REG_6
#define SM_0X0101 SM_REG_7
#define SM_0X000F SM_REG_8
#define SM_0X0F0F SM_REG_9
#define SM_0X0707 SM_REG_10
#define SM_0X5555 SM_REG_11
#define SM_0X3333 SM_REG_12
#define SM_0X00FF SM_REG_13
#define SM_0X001F SM_REG_14
#define SM_0X003F SM_REG_15


/* Flag IDs (in flags Vector Register) */
/* pre-defined flags for the flags vector-register */
#define C_FLAG SM_BIT_0		/* Carry in/out flag */
#define B_FLAG SM_BIT_1		/* Borrow in/out flag */
#define OF_FLAG SM_BIT_2	/* Overflow flag */
#define PE_FLAG SM_BIT_3	/* Parity error */
#define M16_FLAG SM_BIT_4	/* marker with 1 set for (each vector-register-index % 16) == 0 */
#define M256_FLAG SM_BIT_5	/* marker with 1 set for (each vector-register-index % 256) == 0 */
#define M2K_FLAG SM_BIT_6	/* marker with 1 set for (each vector-register-index % 2048) == 0 */

/* Markers for general purpose usage Callee must preserve */
#define GP0_MRK SM_BIT_7
#define GP1_MRK SM_BIT_8
#define GP2_MRK SM_BIT_9
#define GP3_MRK SM_BIT_10
#define GP4_MRK SM_BIT_11

/* Markers for general purpose usage Callee may overwrite preserve */
#define TMP0_MRK SM_BIT_12
#define TMP1_MRK SM_BIT_13
#define TMP2_MRK SM_BIT_14
#define TMP3_MRK SM_BIT_15

// #endif /* __APL_PREPROC__ */




#endif /* APL_DEFS_H */
