/*
 * Copyright (C) 2020, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#include <gsi/libsys.h>
#include <gsi/libsys/log.h>
#include <gsi/libgal.h>
#include <gsi/libgvml_element_wise.h>
#include <stdlib.h>

#include "my_gal_dma.h"
#include "common_dev_host.h"
#define SUCCESS	0
#define FAILURE -1

GAL_INCLUDE_INIT_TASK;

GAL_TASK_ENTRY_POINT(apu_task_4, in, out)
{
	struct cmn_strct_4 *cmn_handle = (struct cmn_strct_4 *)in;

	gvml_init_once();
	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	// Dereference mem_hndl_x (x = 1, 2, 3) to pointers of u16 
	u16 *in_A = gal_mem_handle_to_apu_ptr(cmn_handle->mem_hndl_1);
	u16 *in_B = gal_mem_handle_to_apu_ptr(cmn_handle->mem_hndl_2);
	u16 *out_Z = gal_mem_handle_to_apu_ptr(cmn_handle->mem_hndl_3);
	
	// Choose 3 VRs to use (2 for input, 1 for output)
	enum gvml_vr16 A = GVML_VR16_0;
	enum gvml_vr16 B = GVML_VR16_2;
	enum gvml_vr16 Z = GVML_VR16_5;

	// Choose 1 VMR to use a intermediate
	enum gvml_vm_reg vm_reg0 = GVML_VM_0;
	u16 num_vmrs = 1;
	// Load data from L4 to MMB
	my_dma_l4_to_l1(in_A, vm_reg0, num_vmrs);	
	gvml_load_16(A, vm_reg0);
	my_dma_l4_to_l1(in_B, vm_reg0, num_vmrs);	
	gvml_load_16(B, vm_reg0);

	// Z = A XNOR B
	gvml_xor_16(Z, A, B);
	gvml_not_16(Z, Z);
	 // store back to L4
	gvml_store_16(vm_reg0, Z);
	my_dma_l1_to_l4(out_Z, 1, vm_reg0); 
	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************
 
	return SUCCESS;
}
