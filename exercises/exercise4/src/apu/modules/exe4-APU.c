/*
 * Copyright (C) 2020, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#include <gsi/libsys.h>
#include <gsi/libsys/log.h>
#include <gsi/libgal.h>
#include <gsi/libgvml_element_wise.h>
#include <stdlib.h>

#include "my_gal_dma.h"
#include "common_dev_host.h"
#define SUCCESS	0
#define FAILURE -1

GAL_INCLUDE_INIT_TASK;

GAL_TASK_ENTRY_POINT(apu_task_4, in, out)
{
	struct cmn_strct_4 *cmn_handle = (struct cmn_strct_4 *)in;

	gvml_init_once();
	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	// Dereference mem_hndl_x (x = 1, 2, 3) to pointers of u16 
	
	// Choose 3 VRs to use (2 for input, 1 for output)

	// Choose 1 VMR to use a intermediate

	// Load data from first 2 memory handles from L4 to MMB (via pointer)


	// Z = A XNOR B

	 // store back to L4 to third memory handle (via pointer)


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************
 
	return SUCCESS;
}
