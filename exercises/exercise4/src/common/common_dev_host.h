/*
 * Copyright (C) 2019, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#ifndef EXE_COMMON_H
#define EXE_COMMON_H

struct cmn_strct_4 {
	uint64_t 		mem_hndl_1;
	uint64_t 		mem_hndl_2;
	uint64_t 		mem_hndl_3;
};

#endif /* EXE_COMMON_H */
