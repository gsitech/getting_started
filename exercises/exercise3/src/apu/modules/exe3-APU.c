/*
 * Copyright (C) 2020, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#include <gsi/libsys.h>
#include <gsi/libsys/log.h>
#include <gsi/libgal.h>
#include <gsi/libgvml_element_wise.h>
#include <stdlib.h>

#include "my_gal_dma.h"
#include "common_dev_host.h"
#define SUCCESS	0
#define FAILURE -1
// #define MAX(a,b) ( ((a) > (b)) ? (a) :(b))

GAL_INCLUDE_INIT_TASK;

GAL_TASK_ENTRY_POINT(apu_task_3, in, out)
{
	struct cmn_strct_3 *cmn_handle = (struct cmn_strct_3 *)in;

	gvml_init_once();

	// In order to access pointers we use gal_mem_handle_to_apu_ptr()
	u16 *in_A = gal_mem_handle_to_apu_ptr(cmn_handle->mem_hndl_1);
	u16 *in_B = gal_mem_handle_to_apu_ptr(cmn_handle->mem_hndl_2);
	u16 *out_Z = gal_mem_handle_to_apu_ptr(cmn_handle->mem_hndl_3);

	enum gvml_vr16 A = GVML_VR16_0;
	enum gvml_vr16 B = GVML_VR16_2;
	enum gvml_vr16 Z = GVML_VR16_5;

	enum gvml_vm_reg vm_reg0 = GVML_VM_0;
	u16 num_vmrs = 1;
	// Load data from L4 to MMB
	my_dma_l4_to_l1(in_A, vm_reg0, num_vmrs);	
	gvml_load_16(A, vm_reg0);
	my_dma_l4_to_l1(in_B, vm_reg0, num_vmrs);	
	gvml_load_16(B, vm_reg0);

	// Manipulate data
	gvml_add_u16(Z, A, B);

	 // store back to L4
	gvml_store_16(vm_reg0, Z);
	my_dma_l1_to_l4(out_Z, 1, vm_reg0); 
 
	return SUCCESS;
}
