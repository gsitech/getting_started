/*
 * Copyright (C) 2020, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */
#ifndef __MY_DEV_DMA_H__
#define __MY_DEV_DMA_H__

#include <gsi/libsys/assert.h>
#include <gsi/libgal.h>
#include <gsi/gal-fast-funcs.h>
#include <gsi/libgvml_memory.h>

#define _32K				32768
#define _16K				16384
#define _8K					8192
#define _4K					4096
#define _2K					2048
#define _1K					1024
#define MAX_NUM_VMRS GVML_VM_47

enum {
	_32k_16b_elem = _32K * sizeof(uint16_t),
	_16k_16b_elem = _16K * sizeof(uint16_t)
};

#ifdef APUC_TYPE_sw_sim
#include "my_gal_dma.h"
#else
/**** input ****/

static inline __attribute__((always_inline)) void my_dma_l2_to_l1_32k(enum gvml_vm_reg vmr)
{
	gal_fast_l2dma_l2_ready_rst_all();
	for (uint8_t bank = 0; bank < 4; bank++)
		gvml_load_vmr_16(vmr, bank, bank == (4 - 1), bank << 1);
}

/*indirect */

static inline __attribute__((always_inline)) void my_dma_l4_to_fifo_chunk(
	void *src,
	uint32_t num_vmrs)
{
	struct gal_l4dma_cmd_desc desc = {
		.step_size = _16k_16b_elem,
		.repeat = num_vmrs,
		.l4_step_stride = _32k_16b_elem,
		.num_l4_addrs = 1,
		.APC_l4_addr = {src, (uint8_t *)src + _16k_16b_elem},
                .interleaved_factor = 1,
                .padding = 0,
	};
	gal_fast_set_l4_dma_desc(&desc, GAL_L4DMA_READ);
}

static inline __attribute__((always_inline)) void my_dma_l2_sync(void)
{
	// #if defined(APUC_TYPE_sw_sim)
	// 	return;
	// #elif defined(APUC_TYPE_hw)
		gal_fast_sync_many_blocking( NULL, 0);
	// #else
	// 	#error "Bad APUC_TYPE value"
	// #endif
	// apc0_l2dma_clean_queue();
	// apc1_l2dma_clean_queue();
}

static inline __attribute__((always_inline)) void my_dma_fifo_to_l2_async(void)
{
        struct gal_fast_l2dma_l4_l2_transaction txn = {
                .num_steps = 1,
                .step_size_num_512b_chunk = _16k_16b_elem / 512,
                .l4_addr = NULL,
                .l2_mode = GAL_L2T_MODE_64,
                .l2_col_group = 0,
        };
	
	gal_fast_l2dma_mem_to_l2_start_indirect(0, 1, &txn, GAL_L2DMA_L2_READY_SET);
	gal_fast_l2dma_mem_to_l2_start_indirect(1, 1, &txn, GAL_L2DMA_L2_READY_SET);
}

static inline __attribute__((always_inline)) void fifo_to_l1(enum gvml_vm_reg to_vmr, uint32_t num_vmrs)
{
	GSI_DEBUG_ASSERT(to_vmr + num_vmrs <= MAX_NUM_VMRS);
	for (uint32_t i = to_vmr; i < num_vmrs + to_vmr; ++i) {
		my_dma_fifo_to_l2_async();
		my_dma_l2_sync();
		my_dma_l2_to_l1_32k(i);
	}
}
//************************************************************************
// New by Tomer 24.8.2021:
static inline __attribute__((always_inline)) void my_dma_l4_to_l1_indirect(uint16_t *p16, enum gvml_vm_reg to_vmr, uint16_t num_vmrs) {
	GSI_DEBUG_ASSERT(to_vmr + num_vmrs <= MAX_NUM_VMRS);
	my_dma_l4_to_fifo_chunk(p16, num_vmrs);
	fifo_to_l1(to_vmr, num_vmrs);
}
//************************************************************************

/* direct */

static inline __attribute__((always_inline)) void my_dma_l4_to_l2_16k(int apc_id, uint16_t *p16)
{
	    struct gal_fast_l2dma_l4_l2_transaction txn = {
		.num_steps = 1,
		.step_size_num_512b_chunk = _16k_16b_elem / 512,
		.l4_addr = p16,
		.l2_mode = GAL_L2T_MODE_64,
		.l2_col_group = 0,
	};
	gal_fast_l2dma_mem_to_l2_start(apc_id, 1, &txn, GAL_L2DMA_L2_READY_SET);
}

/* TODO: these are work-around functions */
static inline __attribute__((always_inline)) void my_dma_l4_to_l2_32k(uint16_t *p16)
{
	my_dma_l4_to_l2_16k(0, p16);
	my_dma_l4_to_l2_16k(1, p16 + _16K);
	gal_fast_l2dma_async_memcpy_end(0);
	gal_fast_l2dma_async_memcpy_end(1);
}

static inline __attribute__((always_inline)) void my_dma_l4_to_l1(uint16_t *p16, enum gvml_vm_reg to_vmr, uint16_t num_vmrs) {
	GSI_DEBUG_ASSERT(to_vmr + num_vmrs <= MAX_NUM_VMRS);
	for (uint16_t i = 0; i < num_vmrs; i++) {
		my_dma_l4_to_l2_32k(p16 + _32K * i);
		my_dma_l2_to_l1_32k(to_vmr);
		++to_vmr;
	}
}

/**** output ****/

/* direct */ 
static inline __attribute__((always_inline)) void my_dma_l2_to_l4_16k(int apc_id, uint16_t *p16)
{
	struct gal_fast_l2dma_l4_l2_transaction txn = {
		.num_steps = 1,
		.step_size_num_512b_chunk = _16k_16b_elem / 512,
		.l4_addr = p16,
		.l2_mode = GAL_L2T_MODE_64,
		.l2_col_group = 0,
	};
	gal_fast_l2dma_l2_to_mem_start(apc_id, 1, &txn, GAL_L2DMA_L2_READY_SET);
}

static inline __attribute__((always_inline)) void my_dma_l2_to_l4_32k(uint16_t *p16)
{
	my_dma_l2_to_l4_16k(0, p16);
	my_dma_l2_to_l4_16k(1, p16 + _16K);
	gal_fast_l2dma_async_memcpy_end(0);
	gal_fast_l2dma_async_memcpy_end(1);
}
 
static inline __attribute__((always_inline)) void my_dma_l1_to_l2_32k(enum gvml_vm_reg vmr)
{
	gal_fast_l2dma_l2_ready_rst_all();
	for (uint8_t bank = 0; bank < 4; bank++)
		gvml_store_vmr_16(vmr, bank, bank == (4 - 1), bank * 2);
}
 
static inline __attribute__((always_inline)) void my_dma_l1_to_l4(uint16_t *p16, uint16_t num_vmrs, enum gvml_vm_reg vm_start) {
	for (uint16_t i = 0; i < num_vmrs; i++) {
		my_dma_l1_to_l2_32k(vm_start + i);
		my_dma_l2_to_l4_32k(p16 + _32K * i);
	}
}


/* indirect */

static inline __attribute__((always_inline)) void my_dma_l2_to_fifo_32k(void)
{
        struct gal_fast_l2dma_l4_l2_transaction txn = {
		.num_steps = 1,
		.step_size_num_512b_chunk = _16k_16b_elem / 512,
		.l4_addr = NULL,
		.l2_mode = GAL_L2T_MODE_64,
		.l2_col_group = 0,
	};

	gal_fast_l2dma_l2_to_mem_start_indirect(0, 1, &txn, GAL_L2DMA_L2_READY_SET);
	gal_fast_l2dma_l2_to_mem_start_indirect(1, 1, &txn, GAL_L2DMA_L2_READY_SET);
	// my_dma_l2_sync();
}

//************************************************************************
// New by Tomer 24.8.2021:
static inline __attribute__((always_inline)) void my_dma_fifo_to_l4_chunk(
	void *dst,
	uint32_t num_vmrs)
{
	struct gal_l4dma_cmd_desc desc = {
		.step_size = _16k_16b_elem,
		.repeat = num_vmrs,
		.l4_step_stride = _32k_16b_elem,
		.num_l4_addrs = 1,
		.APC_l4_addr = {dst, (uint8_t *)dst + _16k_16b_elem},
                .interleaved_factor = 1,
                .padding = 0,
	};
	gal_fast_set_l4_dma_desc(&desc, GAL_L4DMA_WRITE);
}

static inline __attribute__((always_inline)) void my_dma_l1_to_l4_indirect(uint16_t *p16, uint16_t num_vmrs, enum gvml_vm_reg vm_start) {
	for (uint16_t i = 0; i < num_vmrs; i++) {
			my_dma_l1_to_l2_32k(vm_start + i);
			my_dma_l2_to_fifo_32k();
	}
	my_dma_l2_sync();
	my_dma_fifo_to_l4_chunk(p16, num_vmrs);
}
//************************************************************************
#endif	/* HW mode */

#endif	/* __MY_DEV_DMA_H__ */
