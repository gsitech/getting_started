/*
 * Copyright (C) 2020, GSI Technology, Inc. All easts reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#include <string.h>
#include <stdio.h>
#include <gsi/libgdl.h>
#include <gsi/libsys.h>
#include <gsi/gsi_sim_config.h>

#include "common_dev_host.h"
#include "exe3_app-defs.h"// #include "gvml_app-defs.h"

#define NUM_CTXS 1

int run_exe3(gdl_context_handle_t ctx_id)
{
	int ret = 0;
	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	
	// Allocate common struct and derefence to host pointer
	size_t buf_size = sizeof(struct cmn_strct_3);
	gdl_mem_handle_t cmn_struct_mem_hndl = gdl_mem_alloc_nonull(ctx_id, buf_size, GDL_CONST_MAPPED_POOL);
	struct cmn_strct_3 *cmn_handle = gdl_mem_handle_to_host_ptr(cmn_struct_mem_hndl);
	// Allocate data in L4 to all input and outputs
	cmn_handle->mem_hndl_1 = gdl_mem_alloc_nonull(ctx_id, 32768 * sizeof(uint16_t), GDL_CONST_MAPPED_POOL);
	cmn_handle->mem_hndl_2 = gdl_mem_alloc_nonull(ctx_id, 32768 * sizeof(uint16_t), GDL_CONST_MAPPED_POOL);
	cmn_handle->mem_hndl_3 = gdl_mem_alloc_nonull(ctx_id, 32768 * sizeof(uint16_t), GDL_CONST_MAPPED_POOL);


	// Initiate data in L4
	uint16_t  *a, *b;
	a = gdl_mem_handle_to_host_ptr(cmn_handle->mem_hndl_1);
	b = gdl_mem_handle_to_host_ptr(cmn_handle->mem_hndl_2);
	for(size_t i = 0 ; i < 32768 ; i ++){
		a[i] = (uint16_t)random();
		b[i] = (uint16_t)random();
	}
	// Start task
	unsigned int task = GDL_TASK(apu_task_3);
	ret = gdl_run_task_timeout(ctx_id, task,
	                           cmn_struct_mem_hndl, GDL_MEM_HANDLE_NULL,
	                           GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE,
	                           GDL_TEMPORARY_DEFAULT_CORE_INDEX, NULL,
	                           0, GDL_USER_MAPPING
	                          );
	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	// Validate results:
	int z_ok = 0;
	{	
		uint16_t  *z;
		z = gdl_mem_handle_to_host_ptr(cmn_handle->mem_hndl_3);
		for(size_t i = 0 ; i < 32768 ; i ++){
			if(!z_ok && z[i] != (uint16_t)(a[i] + b[i]))  { z_ok = 1; printf("Error in computation in element %ld\n", i);}	
		}
	}
	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	// Free memory handles	
	gdl_mem_free(cmn_handle->mem_hndl_1);
	gdl_mem_free(cmn_handle->mem_hndl_2);
	gdl_mem_free(cmn_handle->mem_hndl_3);
	gdl_mem_free(cmn_struct_mem_hndl);
	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************
	
	return ( z_ok);
}

static struct gsi_sim_contexts g_ctxs[NUM_CTXS] = {
	{
		.apu_count = 1,
		.apucs_per_apu = 4,
		.mem_size = 0x40000000,
	}
};

int main(int argc, char *argv[])
{
	
	int ret = 0;
	unsigned int num_ctxs;
	struct gdl_context_desc contexts_desc[GDL_MAX_NUM_CONTEXTS];

	gsi_libsys_init(argv[0], true);

	gsi_sim_create_simulator(NUM_CTXS, g_ctxs);

	gdl_init();
	gdl_context_count_get(&num_ctxs);
	gdl_context_desc_get(contexts_desc, num_ctxs);

	ret = gdl_context_alloc(contexts_desc[0].ctx_id, 0, NULL, NULL);
	if (ret) {
		printf("Failed to allocate GDL context (err = %d)!!!\n", ret);
		return ret;
	}
	run_exe3(contexts_desc[0].ctx_id);

	gdl_context_free(contexts_desc[0].ctx_id);
	gdl_exit();

	gsi_sim_destroy_simulator();
	gsi_libsys_exit();
	if (ret != 0) {
		printf("\nExample 3 App Failed\n");
	} else {
		printf("\nExample 3 App Passed\n");
	}
	return ret;
}
