
#ifndef ___GVML_EXAMPLES_H__
#define ___GVML_EXAMPLES_H__

#include <gsi/libgdl.h>
// #define MAX(a,b)    ( (a>=b) ? (a) : (b) ) 

int run_gvml_and(gdl_context_handle_t ctx_id);
int run_gvml_xnor(gdl_context_handle_t ctx_id);
int run_gvml_nand(gdl_context_handle_t ctx_id);
int run_IO_intro_task1(gdl_context_handle_t ctx_id);
int run_greater_than_with_marker(gdl_context_handle_t ctx_id);
int run_search_task(gdl_context_handle_t ctx_id);
#endif // ___GVML_EXAMPLES_H__