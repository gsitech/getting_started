/*
 * Copyright (C) 2019, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#ifndef GVML_APP_INTERN_H
#define GVML_APP_INTERN_H

struct gvml_app_in_out {
	char 			buffer[64];
	uint16_t		array[32768]; 			
	uint64_t 		in_buffer;
	uint64_t 		out_buffer;
	uint16_t		search_value;
	uint16_t		found_index;
};

#endif /* GVML_APP_INTERN_H */
