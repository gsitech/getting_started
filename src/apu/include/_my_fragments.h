#ifndef ___GVML_H__
#define ___GVML_H__

#include <gsi/libgvml.h>


void _gvml_and_v1(enum gvml_vr16 dst, enum gvml_vr16 src1, enum gvml_vr16 src2);
void _gvml_and_v2(enum gvml_vr16 dst, enum gvml_vr16 src1, enum gvml_vr16 src2);
void _gvml_xnor(enum gvml_vr16 dst, enum gvml_vr16 src1, enum gvml_vr16 src2);
void _gvml_nand(enum gvml_vr16 dst, enum gvml_vr16 src1, enum gvml_vr16 src2);
u16 _gvml_rsp_match(enum gvml_vr16 vr_match, enum gvml_vr16 vr_out);
#endif /* ___GVML_H__ */
