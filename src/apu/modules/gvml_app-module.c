/*
 * Copyright (C) 2020, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#include <gsi/libsys.h>
#include <gsi/libsys/log.h>
#include <gsi/libgal.h>
#include <gsi/libgvml_element_wise.h>
#include <gsi/libgvml_debug.h>
#include <stdlib.h>

// #include "my_dma.h"
#include "my_gal_dma.h"
#include "gvml_app-intern.h"
#include "_my_fragments.h"
#define SUCCESS	0
#define FAILURE -1
// #define MAX(a,b) ( ((a) > (b)) ? (a) :(b))

GAL_INCLUDE_INIT_TASK;

GAL_TASK_ENTRY_POINT(gvml_and, in, out)  
{
	gsi_info("\nRunning %s", ((struct gvml_app_in_out *)in)->buffer);

	gvml_init_once();

	enum gvml_vr16 vreg_in0 = GVML_VR16_0;
	enum gvml_vr16 vreg_in1 = GVML_VR16_1;
	enum gvml_vr16 vreg_out = GVML_VR16_3;

	u16 in1 = 0xaaaa;
	u16 in2 = 0x8421;
	u16 expected_result = (u16) (in1 & in2);
	gsi_info("Performing operation 0x%x AND 0x%x. Expecting result 0x%x", in1, in2, expected_result);
	// load vector registers - all columns to the same value
	gvml_cpy_imm_16(vreg_in0, in1);
	gvml_cpy_imm_16(vreg_in1, in2);

	gal_pm_start();
	// compute AND operation between vector rgisters 
	// Start with gvml functions
	u64 start_instruction =  gal_get_pm_inst_count(true);
	for(int i = 0 ; i < 1000; i++)
		gvml_and_16(vreg_out, vreg_in0, vreg_in1);
	u64 end_instruction =  gal_get_pm_inst_count(true);
	u16 gvml_res = gvml_get_entry_16(vreg_out, 0);
	gsi_info("\nResult gvml function: 0x%x (%llu instructions)", gvml_res, (end_instruction - start_instruction)/1000);
	// APL version 1
	start_instruction =  gal_get_pm_inst_count(true);
	for(int i = 0 ; i < 1000; i++)
		_gvml_and_v1(vreg_out, vreg_in0, vreg_in1);
	end_instruction =  gal_get_pm_inst_count(true);
	gvml_res = gvml_get_entry_16(vreg_out, 0);
	gsi_info("\nResult APL version 1 (multi-bit): 0x%x (%llu instructions)", gvml_res, (end_instruction - start_instruction)/1000 );
	
	// APL version 2
	start_instruction =  gal_get_pm_inst_count(true);
	for(int i = 0 ; i < 1000; i++)
		_gvml_and_v2(vreg_out, vreg_in0, vreg_in1);
	end_instruction =  gal_get_pm_inst_count(true);
	gvml_res = gvml_get_entry_16(vreg_out, 0);
	gsi_info("\nResult APL version 2 (bit-serial): 0x%x (%llu instructions)", gvml_res, (end_instruction - start_instruction)/1000);
	gal_pm_stop();
	// get the result (read column 0)
	if (gvml_res == (in1 & in2))
		return SUCCESS;
	else
		return FAILURE;
}

GAL_TASK_ENTRY_POINT(gvml_xnor, in, out)
{
	gsi_info("\nRunning %s", ((struct gvml_app_in_out *)in)->buffer);

	gvml_init_once();

	enum gvml_vr16 vreg_in0 = GVML_VR16_0;
	enum gvml_vr16 vreg_in1 = GVML_VR16_1;
	enum gvml_vr16 vreg_out = GVML_VR16_3;

	u16 in1 = 0xaaaa;
	u16 in2 = 0x8421;
	u16 expected_result = (u16) ~(in1 ^ in2);
	gsi_info("Performing operation 0x%x XNOR 0x%x. Expecting result 0x%x", in1, in2, expected_result);
	// load vector registers - all columns to the same value
	gvml_cpy_imm_16(vreg_in0, in1);
	gvml_cpy_imm_16(vreg_in1, in2);

	// compute XNOR operation between vector rgisters 
	// Start with gvml functions
	gvml_xor_16(vreg_out, vreg_in0, vreg_in1);
	gvml_not_16(vreg_out, vreg_out);
	u16 gvml_res = gvml_get_entry_16(vreg_out, 0);
	// APL version 
	_gvml_xnor(vreg_out, vreg_in0, vreg_in1);
	u16 _gvml_res = gvml_get_entry_16(vreg_out, 0);
	if(gvml_res ==  _gvml_res)
		gsi_info("\nGreat! Result gvml function: 0x%x \nResult APL version : 0x%x", gvml_res, _gvml_res);
	if(gvml_res !=  _gvml_res)	
		gsi_info("\nERROR! Result gvml function: 0x%x \nResult APL version : 0x%x\n (Expected value for both=  0x%x)", gvml_res, _gvml_res, expected_result);
	if (gvml_res == expected_result)
		return SUCCESS;
	else
		return FAILURE;
}

GAL_TASK_ENTRY_POINT(gvml_nand, in, out)
{
	gsi_info("\nRunning %s", ((struct gvml_app_in_out *)in)->buffer);

	gvml_init_once();

	enum gvml_vr16 vreg_in0 = GVML_VR16_0;
	enum gvml_vr16 vreg_in1 = GVML_VR16_1;
	enum gvml_vr16 vreg_out = GVML_VR16_3;

	u16 in1 = 0xabcd;
	u16 in2 = 0x8421;
	u16 expected_result = (u16) ~(in1 & in2);
	gsi_info("Performing operation 0x%x NAND 0x%x. Expecting result 0x%x", in1, in2, expected_result);
	// load vector registers - all columns to the same value
	gvml_cpy_imm_16(vreg_in0, in1);
	gvml_cpy_imm_16(vreg_in1, in2);

	// compute NAND operation between vector rgisters 
	// Start with and_u16 and follow up with not
	gvml_and_16(vreg_out, vreg_in0, vreg_in1);
	gvml_not_16(vreg_out, vreg_out);
	u16 gvml_res = gvml_get_entry_16(vreg_out, 0);
	gsi_info("\nResult gvml function: 0x%x ", gvml_res);
	// APL version 
	_gvml_nand(vreg_out, vreg_in0, vreg_in1);
	gvml_res = gvml_get_entry_16(vreg_out, 0);
	gsi_info("\nResult APL version : 0x%x ", gvml_res);
	// get the result (read column 0)
	if (gvml_res == (u16)~(in1 & in2))
		return SUCCESS;
	else
		return FAILURE;
}

GAL_TASK_ENTRY_POINT(gvml_IO_intro1, in, out)
{
	struct gvml_app_in_out *cmn_handle = (struct gvml_app_in_out *)in;
	gsi_info("\nRunning %s", cmn_handle->buffer);

	gvml_init_once();
	u16 test_index = 5;

	// Access data in the struct as normal
	u16 *p_array_dev = cmn_handle->array;
	gsi_info("Printing first 5 elements from p_array_dev:");
	for(size_t i = 0 ; i < 5; i++) 
		gsi_info("p_array_dev[%d] = %d", i, p_array_dev[i]);
	u16 test_value = p_array_dev[test_index];

	// In order to access pointers we use gal_mem_handle_to_apu_ptr()
	u32 *p_u32 = gal_mem_handle_to_apu_ptr(cmn_handle->in_buffer);
	gsi_info("Printing first 5 elements from p_u32:");
	for(size_t i = 0 ; i < 5; i++) 
		gsi_info("p_u32[%d] = %d", i, p_u32[i]);


	enum gvml_vm_reg vm_reg0 = GVML_VM_0;
	uint num_vmrs = 1;
	// Loading data from L4 to the APU L1 memory:
	// gsi_info("loading from L4 to L1");
	my_dma_l4_to_l1(p_array_dev, vm_reg0, (u16)num_vmrs);		// Also can use my_dma_l4_to_l1_indirect() - which is faster- but only works for HW
	// Load data from L1 memory to MMB:
	enum gvml_vr16 vreg_in0 = GVML_VR16_0;
	gvml_load_16(vreg_in0, vm_reg0);

	// Manipulate data
	gvml_not_16(vreg_in0, vreg_in0); 		// vreg0 = ~vreg0
	
	// Store data from MMB to L1
	gvml_store_16(vm_reg0, vreg_in0);
	// gsi_info("loading from L1 to L4");

	// Store from L1 to L4
	my_dma_l1_to_l4(p_array_dev, 1, vm_reg0);
	gsi_info("Printing first 5 elements from p_array_dev after NOT operation:");
	for(size_t i = 0 ; i < 10; i++) 
		gsi_info("APU Output[%d] = %d", i, p_array_dev[i]);
	

	if (test_value == (u16)~p_array_dev[test_index] )
		return SUCCESS;
	else
		return FAILURE;
}

GAL_TASK_ENTRY_POINT(gvml_IO_intro2, in, out)
{
	struct gvml_app_in_out *cmn_handle = (struct gvml_app_in_out *)in;
	gsi_info("\nRunning %s", cmn_handle->buffer);

	gvml_init_once();

	// In order to access pointers we use gal_mem_handle_to_apu_ptr()
	u16 *in_data = gal_mem_handle_to_apu_ptr(cmn_handle->in_buffer);
	u16 *out_data = gal_mem_handle_to_apu_ptr(cmn_handle->out_buffer);


	enum gvml_vm_reg vm_reg0 = GVML_VM_0;
	uint num_vmrs = 1;
	// Loading data from L4 to the APU L2 memory:
	my_dma_l4_to_l2_16k(0, in_data); 			// Start L2DMA operation to APC0
	my_dma_l4_to_l2_16k(1, in_data + _16K);		// Start L2DMA operation to APC1
	// Now we can do other ARC/MMB operations while the L2DMAs are working. 
	// Do some other work
	gal_fast_l2dma_async_memcpy_end(0);			// Make sure L2DMA for APC0 finished
	gal_fast_l2dma_async_memcpy_end(1);			// Make sure L2DMA for APC1 finished
	my_dma_l2_to_l1_32k(vm_reg0);				// Load the data from L2 to L1
	// Load the data from L1 memory to MMB:
	enum gvml_vr16 vreg_in0 = GVML_VR16_0;
	gvml_load_16(vreg_in0, vm_reg0);

	// Manipulate data
	gvml_not_16(vreg_in0, vreg_in0); 		// vreg0 = ~vreg0
	
	// Store data from MMB to L1
	gvml_store_16(vm_reg0, vreg_in0);

	// Store from L1 to L2
	my_dma_l1_to_l2_32k(vm_reg0);
	// Store from L2 to L4
	my_dma_l2_to_l4_16k(0, out_data);
	my_dma_l2_to_l4_16k(1, out_data + _16K);
	// Now we can do other ARC/MMB operations while the L2DMAs are working. 
	gal_fast_l2dma_async_memcpy_end(0);		// Make sure L2DMA for APC0 finished 
	gal_fast_l2dma_async_memcpy_end(1);		// Make sure L2DMA for APC1 finished 

	// Output verification is done on host- exit
	return SUCCESS;
}

GAL_TASK_ENTRY_POINT(gvml_gt_marker, in, out)
{
	// Not compatiable for simulator
	int ret = SUCCESS;
	gvml_init_once();

	enum gvml_vr16 vreg_in0 = GVML_VR16_0;
	enum gvml_vr16 vreg_in1 = GVML_VR16_1;
	enum gvml_vr16 vreg_out = GVML_VR16_3;
	enum gvml_mrks_n_flgs marker0 = GVML_MRK0;
	struct gvml_app_in_out *cmn_handle = (struct gvml_app_in_out *)in;
	gsi_info("\nRunning %s", cmn_handle->buffer);
	u16 *p_in1 = gal_mem_handle_to_apu_ptr(cmn_handle->in_buffer); 
	u16 *p_in2 = cmn_handle->array;
	// Load data to MMB
	my_dma_l4_to_l1(p_in1, GVML_VM_0, 1);
	my_dma_l4_to_l1(p_in2, GVML_VM_1, 1);
	gvml_load_16(vreg_in0, GVML_VM_0);
	gvml_load_16(vreg_in1, GVML_VM_1); 
	//vreg_out = MAX(vreg_in0, vreg_in1);
	// compute vreg0 > vreg1, store boolean result in marker 
	gvml_gt_u16(marker0, vreg_in0, vreg_in1);
	gvml_cpy_16_mrk(vreg_out, vreg_in0, marker0);
	// vreg_out = vreg0 if MAX(verg_in0, verg_in1);
	gvml_not_m(marker0, marker0); // marker = !marker
	gvml_cpy_16_mrk(vreg_out, vreg_in1, marker0);

	// Store result from MMB to L4
	enum gvml_vm_reg vm_reg0 = GVML_VM_0;
	gvml_store_16(vm_reg0, vreg_out );
	u16* out_array = gal_mem_handle_to_apu_ptr(cmn_handle->out_buffer);
	my_dma_l1_to_l4(out_array, 1, vm_reg0);
	// Result verification is done by host!
	return ret;
}

GAL_TASK_ENTRY_POINT(gvml_search_cam, in, out)
{
	struct gvml_app_in_out *cmn_handle = (struct gvml_app_in_out *)in;
	gsi_info("\nRunning %s", cmn_handle->buffer);

	gvml_init_once();

	// In order to access pointers we use gal_mem_handle_to_apu_ptr()
	u16 *search_data = (u16*)gal_mem_handle_to_apu_ptr(cmn_handle->in_buffer);

	enum gvml_vr16 vreg_in0 = GVML_VR16_0;
	enum gvml_vr16 vreg_in1 = GVML_VR16_1;
	enum gvml_vr16 vreg_out = GVML_VR16_2;
	enum gvml_vm_reg vm_reg0 = GVML_VM_0;
	uint num_vmrs = 1;
	// ------------- Loading data from L4 to the APU: ----------------------------------
	// Loading from L4 to L1
	my_dma_l4_to_l1(search_data, vm_reg0, (u16)num_vmrs);		
	// Load data from L1 memory to MMB:
	gvml_load_16(vreg_in0, vm_reg0);

	// -------------- Search data -------------------------------------------------------
	gsi_info("Searching value: %d", cmn_handle->search_value);
	// Copy the searched value to all columns in APU
	gvml_cpy_imm_16(vreg_in1, cmn_handle->search_value);
	// If search_value exits in column i, vreg_out will hold the value 0xffff. Else, a different value is stored.
	_gvml_xnor(vreg_out, vreg_in0, vreg_in1);				

	// Output the index of column with value 0xffff in vreg_out, if none exist, return 0xffff;
	u16 match_id = _gvml_rsp_match(vreg_out, GVML_VR16_IDX);		
	cmn_handle->found_index = match_id;
	if (match_id != 0xffff)
	{	
		return SUCCESS;
	}
	else{
		return FAILURE;
	}
	
}
