/*
 * Copyright (C) 2021, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

APL_C_INCLUDE <gsi/libsys.h>
APL_C_INCLUDE <gsi/libapl.h>
APL_C_INCLUDE <gsi/libgvml.h>
void _gvml_and_v1(enum gvml_vr16 dst, enum gvml_vr16 src1, enum gvml_vr16 src2);
void _gvml_and_v2(enum gvml_vr16 dst, enum gvml_vr16 src1, enum gvml_vr16 src2);
void _gvml_xnor(enum gvml_vr16 dst, enum gvml_vr16 src1, enum gvml_vr16 src2);
void _gvml_nand(enum gvml_vr16 dst, enum gvml_vr16 src1, enum gvml_vr16 src2);
u16 _gvml_rsp_match(enum gvml_vr16 vr_match, enum gvml_vr16 vr_out);

#include "gvml_apl_defs.apl.h"
//------------------------------------------------------ OR functions ------------------------------------------------------
APL_FRAG frag_or(RN_REG dst,RN_REG src1,RN_REG src2)
{ 
	SM_0XFFFF: RL = SB[src1]; 	// RL = src1 
	SM_0XFFFF: RL |= SB[src2]; 	// RL = src1 | src2
	SM_0XFFFF: SB[dst] = RL; 
};

//------------------------------------------------------ AND functions ------------------------------------------------------
APL_FRAG frag_and_v1(RN_REG dst,RN_REG src1,RN_REG src2)
{
	SM_0XFFFF: RL = SB[src1, src2]; 	// RL = src1 AND src2
	SM_0XFFFF: SB[dst] = RL; 
};

void _gvml_and_v1(enum gvml_vr16 dst, enum gvml_vr16 src1, enum gvml_vr16 src2)
{// multi-bit
	apl_set_rn_reg(RN_REG_0, dst);
	apl_set_rn_reg(RN_REG_1, src1);
	apl_set_rn_reg(RN_REG_2, src2);
	RUN_FRAG_ASYNC(frag_and_v1( dst = RN_REG_0, src1 = RN_REG_1, src2 = RN_REG_2));
}  
// *********************************************************************************
APL_FRAG frag_and_v2(RN_REG dst_rn,RN_REG src1,RN_REG src2)
{
	SM_0XFFFF: RL = SB[src1]; 	// RL = src1 
	SM_0XFFFF: RL &= SB[src2]; 	// RL = src1 & src2
	SM_0XFFFF: SB[dst_rn] = RL; 
};


void _gvml_and_v2(enum gvml_vr16 dst, enum gvml_vr16 src1, enum gvml_vr16 src2)
{ // bit-serial
	apl_set_rn_reg(RN_REG_0, dst);
	apl_set_rn_reg(RN_REG_1, src1);
	apl_set_rn_reg(RN_REG_2, src2);
	RUN_FRAG_ASYNC(frag_and_v2( dst_rn = RN_REG_0,src1 = RN_REG_1, src2 = RN_REG_2));
}
//------------------------------------------------------ XNOR functions ------------------------------------------------------

APL_FRAG frag_xnor(RN_REG dst,RN_REG src1,RN_REG src2)
{
	SM_0XFFFF: RL = SB[src1]; 					// RL = src1 
	SM_0XFFFF: RL ^= SB[src2] ;				 	// RL = src1 XOR src2
	SM_0XFFFF: SB[dst] = INV_RL; 
	 
};

void _gvml_xnor(enum gvml_vr16 dst, enum gvml_vr16 src1, enum gvml_vr16 src2)
{
	apl_set_rn_reg(RN_REG_0, dst);
	apl_set_rn_reg(RN_REG_1, src1);
	apl_set_rn_reg(RN_REG_2, src2);
	RUN_FRAG_ASYNC(frag_xnor( dst = RN_REG_0,src1 = RN_REG_1, src2 = RN_REG_2));
}

//------------------------------------------------------ NAND functions ------------------------------------------------------

APL_FRAG frag_nand(RN_REG dst,RN_REG src1, RN_REG src2)
{
	SM_0XFFFF: RL = ~SB[src1, src2] & INV_RSP16; 	// RL = ~(src1 & src2 ) | '1'
	SM_0XFFFF: SB[dst] = RL; 
};

void _gvml_nand(enum gvml_vr16 dst, enum gvml_vr16 src1, enum gvml_vr16 src2)
{
	apl_set_rn_reg(RN_REG_0, dst);
	apl_set_rn_reg(RN_REG_1, src1);
	apl_set_rn_reg(RN_REG_2, src2);
	RUN_FRAG_ASYNC(frag_nand( dst = RN_REG_0,src1 = RN_REG_1, src2 = RN_REG_2));
}

//------------------------------------------------------ Search and match functions ------------------------------------------------------

APL_FRAG frag_match_n_rsp(RN_REG match, RN_REG out)
{
	{ // GL = 1 if match = 0xffff
		SM_0XFFFF: RL = SB[match];
		SM_0XFFFF: GL = RL;
	}
	{	// SM_0X7FFF RL = SB[out] if GL == 1.
		// SM_0X8000 RL = GL;
		SM_0X00FF: RL = SB[out] & GL;
		SM_0X003F << 8: RL = SB[out] & GL;
		SM_0X0001 << 14: RL = SB[out] & GL;
		SM_0X0001 << 15: RL = GL;
	}
	// output the required data over rsp
	SM_0XFFFF: RSP16 = RL;
	RSP256 = RSP16;
	RSP2K = RSP256;
	RSP32K = RSP2K;
	NOOP;
	NOOP;
	RSP_END;
};

u16 _gvml_rsp_match(enum gvml_vr16 vr_match, enum gvml_vr16 vr_out)
{
	const size_t num_banks = 4; 
	const size_t num_apcs = 2;
	u32 match = 0xffff;
	u32 match_final = 0xffff;
	apl_set_rn_reg(RN_REG_0, vr_match);
	apl_set_rn_reg(RN_REG_1, vr_out);
	RUN_FRAG_ASYNC(frag_match_n_rsp( match = RN_REG_0, out = RN_REG_1));
	
	for(size_t apc_id = 0 ; apc_id < num_apcs; apc_id++){
		apl_rsp_rd(apc_id); // you must read both APCs or you will crash
		u8 rsp32k = apl_rd_rsp32k_reg();
		if(rsp32k)
		{
			for(size_t i = 0 ; i < num_banks; i++)
			{
				match = apl_rd_rsp2k_reg(i);
				if(match){ //take correct halfbank
					match_final = (match & (0x8000 ) ?  match : (match >> 16) ) & 0x7fff;
					break;
				}
			}
		}
	}
	return (u16)match_final;
}

