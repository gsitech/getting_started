#include <string.h>
#include <stdio.h>
#include <gsi/libgdl.h>
#include <gsi/libsys.h>

#include "gvml_examples.h"
#include "gvml_app-intern.h"
#include "dv_my_gvml_app-defs.h"

/*
MMB Boolean operation example
ARC write two u16 values to MMB variables
MMB do bitwise AND between them to another u16 variable
ARC read the output from MMB and verify correct result.
What's new:
* Associative AND operation on the RBL (wire)
* Use of MMB performance monitor
APU code:
* GAL library
* gvml_init_once
* gvml_cpy_imm_16
* gvml_get_entry_16
* gvml_and_16 (multi-bit read)
* _gvml_and_v1 (multi-bit)
* _gvml_and_v2 (bit-serial)
* gal_pm_start
* gal_get_pm_inst_count
* gal_pm_stop
*/
int run_gvml_and(gdl_context_handle_t ctx_id)
{
	int ret = 0;
	unsigned int task = GDL_TASK(gvml_and);
	size_t buf_size = sizeof(struct gvml_app_in_out);
	gdl_mem_handle_t dev_buf = gdl_mem_alloc_nonull(ctx_id, buf_size, GDL_CONST_MAPPED_POOL);
	struct gvml_app_in_out *cmn_handle = gdl_mem_handle_to_host_ptr(dev_buf);

	strcpy(cmn_handle->buffer, "AND example App");

	ret = gdl_run_task_timeout(ctx_id, task,
	                           dev_buf, GDL_MEM_HANDLE_NULL,
	                           GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE,
	                           GDL_TEMPORARY_DEFAULT_CORE_INDEX, NULL,
	                           0, GDL_USER_MAPPING
	                          );

	gdl_mem_free(dev_buf);
	return ret;
}

/*
MMB Boolean operation example
ARC write two u16 values to MMB variables
MMB do bitwise XNOR between them to another u16 variable
ARC read the output from MMB and verify correct result.
Example content
Host (x86) code:
* C code
* GDL library
APU (ARC, MMB) code:
* C code
* GVML library
* Utility library
* APL code (XNOR function under test)
Test flow:
Host: Sends the task to APU (calls the APU to run the task)
* gdl_init and gdl_exit
* gdl_context_alloc and gdl_context_free
* gdl_run_task_timeout
* task code
* task data
APU: perform the task and return to host
* gsi_info
* variable declerations (types)
* gvml_xor_16
* gvml_not_16
* _gvml_xnor (APL code, function under test)
Host: Terminate the program
*/
int run_gvml_xnor(gdl_context_handle_t ctx_id)
{
	int ret = 0;
	unsigned int task = GDL_TASK(gvml_xnor);
	size_t buf_size = sizeof(struct gvml_app_in_out);
	gdl_mem_handle_t dev_buf = gdl_mem_alloc_nonull(ctx_id, buf_size, GDL_CONST_MAPPED_POOL);
	struct gvml_app_in_out *cmn_handle = gdl_mem_handle_to_host_ptr(dev_buf);

	strcpy(cmn_handle->buffer, "XNOR example App");

	ret = gdl_run_task_timeout(ctx_id, task,
	                           dev_buf, GDL_MEM_HANDLE_NULL,
	                           GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE,
	                           GDL_TEMPORARY_DEFAULT_CORE_INDEX, NULL,
	                           0, GDL_USER_MAPPING
	                          );

	gdl_mem_free(dev_buf);
	return ret;
}

/* similar to XNOR */
int run_gvml_nand(gdl_context_handle_t ctx_id)
{
	int ret = 0;
	unsigned int task = GDL_TASK(gvml_nand);
	size_t buf_size = sizeof(struct gvml_app_in_out);
	gdl_mem_handle_t dev_buf = gdl_mem_alloc_nonull(ctx_id, buf_size, GDL_CONST_MAPPED_POOL);
	struct gvml_app_in_out *cmn_handle = gdl_mem_handle_to_host_ptr(dev_buf);

	strcpy(cmn_handle->buffer, "NAND example App");

	ret = gdl_run_task_timeout(ctx_id, task,
	                           dev_buf, GDL_MEM_HANDLE_NULL,
	                           GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE,
	                           GDL_TEMPORARY_DEFAULT_CORE_INDEX, NULL,
	                           0, GDL_USER_MAPPING
	                          );

	gdl_mem_free(dev_buf);
	return ret;
}

/*
Data path: Parallel, synchronous I/O example
I/O functions
* Host to device (system memory to device memory, L4)
* Input (APU inbound)
	* Read data L4 to L2 using L2DMA
	* Copy data L2 to L1
	* Load data L1 to MMB
* Output (APU outbound)
	* Store data MMB to L1
	* Copy data L1 to L2
	* Write data L2 to L4 using L2DMA
	* Device to host (device memory, L4 to system memory)

* Host code:
 	1) Allocate buffer in L4 memory for an array in the common struct.
	   Initialize the buffer by writing directly to the array.
 	2) Allocate buffer in L4 memory for an array using memory handle.
	   Convert handle to host pointer, write directly to the array using the pointer.	
* APU code:
 	1) Access data from L4.
	Load data from L4 to MMB using L2DMA.
 	2) Convert memory handle to APU pointer
What's new:
Host side:
* Common struct for APU and host.
* Allocate data on device memory.
* Using memory handles.
* Store data to device memory using pointers.
* gdl_mem_alloc
* gdl_mem_free
* gdl_mem_handle_to_host_ptr
APU side:
* Access data from L4.
* Loading data from L4 to MMB.
* Storing data from MMB to L4.
* gal_mem_handle_to_apu_ptr
* my_dma_l4_to_l1
* my_dma_l1_to_l4


*/
int run_IO_intro_task1(gdl_context_handle_t ctx_id)
{
	int ret = 0;
	printf("Starting I\\O introduction task 1\n");
	unsigned int task = GDL_TASK(gvml_IO_intro1);
	size_t buf_size = sizeof(struct gvml_app_in_out);
	gdl_mem_handle_t dev_buf = gdl_mem_alloc(ctx_id, buf_size, GDL_CONST_MAPPED_POOL);
	struct gvml_app_in_out *cmn_handle = gdl_mem_handle_to_host_ptr(dev_buf);
	// "array" was allocated within the struct. We can access memory directly
	uint16_t *p_array = cmn_handle->array;
	for(size_t i = 0 ; i < 32768; i++){
		p_array[i] = i;
	}
	// Allocate memory consisiting 5000 unsigned ints (32 bits each) 
	buf_size = 5000 * sizeof(uint32_t);
	gdl_mem_handle_t dev_buf_u32 = gdl_mem_alloc_nonull(ctx_id, buf_size, GDL_CONST_MAPPED_POOL); // allocate the memory
	// Notice that dev_buf_u32 is an virtual address for the memory.
	// If we'd like to access the memory we must use gdl_mem_handle_to_host_ptr() to retrieve a pointer
	uint32_t *p_u32 = gdl_mem_handle_to_host_ptr(dev_buf_u32);
	for(size_t i = 0 ; i < 5000; i++){
		p_u32[i] = 5*i;
	}

	// In order to access the memory by device we store the dev_buf (pointer) in the struct sent to the device:
	cmn_handle->in_buffer = dev_buf_u32;

	strcpy(cmn_handle->buffer, "IO Intro App 1");


	ret = gdl_run_task_timeout(ctx_id, task,
	                           dev_buf, GDL_MEM_HANDLE_NULL,
	                           GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE,
	                           GDL_TEMPORARY_DEFAULT_CORE_INDEX, NULL,
	                           0, GDL_USER_MAPPING
	                          );

	gdl_mem_free(dev_buf_u32);
	gdl_mem_free(dev_buf);
	printf("Finished I\\O introduction task 1\n\n");
	return ret;
}

/*
Data path: Parallel, asynchronous I/O example
I/O functions
* Host to device (Utilizing GDL library )
* Input (APU inbound)
	* Async Input (APU inbound)
	* Read data L4 to L2 using L2DMA (asynchronous)
	* Sync L2DMA
	* Copy data L2 to L1
* Output (APU outbound)
	* Store data MMB to L1
	* Copy data L1 to L2
	* Write data L2 to L4 using L2DMA (asynchronous)
	* Sync L2DMA
	* Device to host (device memory, L4 to system memory)

* Host code:
 	* Initialize array in system memory of 32K u16 elements
	* Mem Copy input array from system memory to L4 
	* Mem Copy output array from L4 to system memory 
	
* APU code:
 	Async Load data from L4 to L1 async
 	Async store data from L1 to L4 async
  
What's new:
Host side:
* Coping data to/from APU using GDL library.
* Accessing APU output.
* gdl_mem_cpy_to_dev
* gdl_mem_cpy_from_dev
APU side:
* Loading data from L4 to MMB using asynchronous functions, allowing parallelizing I/O with ARC or APU operation.
* Storing data from MMB to L4 using asynchronous functions, allowing parallelizing I/O with ARC or APU operation.
* my_dma_l4_to_l2_16k
* gal_fast_l2dma_async_memcpy_end
* my_dma_l2_to_l1_32k
* my_dma_l1_to_l2_32k
* my_dma_l2_to_l4_16k
*/
int run_IO_intro_task2(gdl_context_handle_t ctx_id)
{
	int ret = 0;
	printf("Starting I\\O introduction task 2\n");
	unsigned int task = GDL_TASK(gvml_IO_intro2);
	size_t buf_size = sizeof(struct gvml_app_in_out);
	gdl_mem_handle_t dev_buf = gdl_mem_alloc(ctx_id, buf_size, GDL_CONST_MAPPED_POOL);
	struct gvml_app_in_out *cmn_handle = gdl_mem_handle_to_host_ptr(dev_buf);
	// Allocate memory for input and output. Consisiting 32768 unsigned short (16 bits each) 
	buf_size = 32768 * sizeof(uint16_t);
	cmn_handle->in_buffer = gdl_mem_alloc_nonull(ctx_id, buf_size, GDL_CONST_MAPPED_POOL); // allocate memory for input
	cmn_handle->out_buffer = gdl_mem_alloc_nonull(ctx_id, buf_size, GDL_CONST_MAPPED_POOL); // allocate memory for output
	// Recall that for accessing the memory we can use gdl_mem_handle_to_host_ptr() to retrieve a pointer
	/* // We can directly write to memory by accessing the memory using memory handle
		uint16_t *p_u16 = gdl_mem_handle_to_host_ptr(cmn_handle->in_buffer);
		for(uint16_t i = 0 ; i < 32768; i++){ p_u16[i] = i;} 
	*/

	// Alternatively, we can copy to the allocated buffer from system memory using GDL library:
	// First, we allocate and initailize the system memory (This the input to our program).
	uint16_t *sys_mem_input = malloc(32768 * sizeof(uint16_t));
	for(size_t i = 0 ; i < 32768; i++){ sys_mem_input[i] = i; }
	// Next, we use the GDL library to store the data directly to memory. If the host has a DMA, this function utilize it.
	gdl_mem_cpy_to_dev(cmn_handle->in_buffer, sys_mem_input, 32768 * sizeof(uint16_t));
	
	strcpy(cmn_handle->buffer, "IO Intro App 2");


	ret = gdl_run_task_timeout(ctx_id, task,
	                           dev_buf, GDL_MEM_HANDLE_NULL,
	                           GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE,
	                           GDL_TEMPORARY_DEFAULT_CORE_INDEX, NULL,
	                           0, GDL_USER_MAPPING
	                          );
	
	// Once the task is completed, we can copy the output from the device with GDL
	uint16_t *sys_mem_output = malloc(32768 * sizeof(uint16_t));
	gdl_mem_cpy_from_dev(sys_mem_output, cmn_handle->out_buffer, 32768 * sizeof(uint16_t));

	// Test whether the output is correct. We expect input[i] = ~output[i]
	for(size_t i = 0 ; i < 32768; i++){
		if(sys_mem_output[i] != (uint16_t) ~sys_mem_input[i]){
			printf("Error: sys_mem_output[%ld] (%x) != (uint16_t) ~sys_mem_input[%ld] (%x)\n", i, sys_mem_output[i],
							i, sys_mem_input[i]);
			ret = 1;
		}
	}
	if(!ret){
		printf("Recieved expected output by host!\n");
	}

	free(sys_mem_output);
	free(sys_mem_input);
	gdl_mem_free(cmn_handle->in_buffer);
	gdl_mem_free(cmn_handle->out_buffer);
	gdl_mem_free(dev_buf);
	printf("Finished I\\O introduction task 2\n\n");
	return ret;
}

/*
Example of End to End application for elementwise MAX(a,b) operation between 2 vectors "a" and "b" of unsigned 16 bit numbers
ARC loads 2 vectors given from host to VRs in MMB
APU performs element-wise greater-than operation and stores result in another VR
ARC outputs the result to L4 for the host to read and verify result
What's new:
APU side:
* Using GVML markers
* First example of end-to-end application
* gvml_gt_u16
* gvml_cpy_16_mrk
* gvml_not_m
*/
int run_greater_than_with_marker(gdl_context_handle_t ctx_id)
{
	int ret = 0;
	printf("Starting Greater Than wth marker task.\n");
	unsigned int task = GDL_TASK(gvml_gt_marker);
	size_t buf_size = sizeof(struct gvml_app_in_out);
	gdl_mem_handle_t dev_buf = gdl_mem_alloc_nonull(ctx_id, buf_size, GDL_CONST_MAPPED_POOL);
	struct gvml_app_in_out *cmn_handle = gdl_mem_handle_to_host_ptr(dev_buf);
	uint16_t *p_array = cmn_handle->array;
	for(uint16_t i = 0 ; i < 32768; i++){
		p_array[i] = (uint16_t) random();
	}
	// Allocate memory consisiting 32768 unsigned shorts (16 bits each) 
	buf_size = 32768 * sizeof(uint16_t);
	gdl_mem_handle_t dev_buf_u16 = gdl_mem_alloc_nonull(ctx_id, buf_size, GDL_CONST_MAPPED_POOL); // allocate the memory
	// If we'd like to access the memory we must use gdl_mem_handle_to_host_ptr() to retrieve a pointer
	uint16_t *p_u16 = gdl_mem_handle_to_host_ptr(dev_buf_u16);
	for(uint16_t i = 0 ; i < 32768; i++){
		p_u16[i] = i /* (uint16_t)random() */;
	}

	// In order to access the memory by device we store the dev_buf (pointer) in the struct sent to the device:
	cmn_handle->in_buffer = dev_buf_u16;
	cmn_handle->out_buffer = gdl_mem_alloc_nonull(ctx_id, buf_size, GDL_CONST_MAPPED_POOL); // allocate memory for output

	strcpy(cmn_handle->buffer, "Greater Than with marker example App");
	ret = gdl_run_task_timeout(ctx_id, task,
	                           dev_buf, GDL_MEM_HANDLE_NULL,
	                           GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE,
	                           GDL_TEMPORARY_DEFAULT_CORE_INDEX, NULL,
	                           0, GDL_USER_MAPPING
	                          );

	u16 *APU_out = gdl_mem_handle_to_host_ptr(cmn_handle->out_buffer);
	// Check if result is correct
	for(size_t i = 0 ; i < 32768; i++){
		if(APU_out[i] != MAX(p_u16[i], p_array[i])){
			// printf("Error: output[%ld] (%d) !=  MAX(p_u16[%ld], p_array[%ld]) (%d)\n", i, APU_out[i],
			// 				i,i, MAX(p_u16[i], p_array[i]) );
			ret = 1;
		}
		else if( i < 2000){
			printf("Output[%ld]: %d =  MAX(%d, %ld)\n", i, APU_out[i],
							p_array[i],p_u16[i]);
		}
	}
	if(!ret){
		printf("Recieved expected output by host!\n");
	}

	gdl_mem_free(dev_buf);
	printf("Finished Greater Than wth marker task.\n\n");
	return ret;
}

int run_search_task(gdl_context_handle_t ctx_id)
{
	int ret = 0;
	// Initialization and allocation of data:
	unsigned int task = GDL_TASK(gvml_search_cam);
	size_t buf_size = sizeof(struct gvml_app_in_out);
	gdl_mem_handle_t dev_buf = gdl_mem_alloc_nonull(ctx_id, buf_size, GDL_CONST_MAPPED_POOL);
	struct gvml_app_in_out *cmn_handle = gdl_mem_handle_to_host_ptr(dev_buf);
	// Allocate memory consisiting 32K unsigned shorts (16 bits each) 
	buf_size = 32768 * sizeof(uint16_t);
	gdl_mem_handle_t dev_buf_u16= gdl_mem_alloc_nonull(ctx_id, buf_size, GDL_CONST_MAPPED_POOL); // allocate the memory
	
	// Initializing the allicated array
	uint16_t *p_u16 = (uint16_t*) gdl_mem_handle_to_host_ptr(dev_buf_u16);
	for(uint16_t i = 0 ; i < 32768; i++){
		p_u16[i] = 32767 - i;
	}

	// In order to access the memory by device we store the dev_buf (pointer) in the struct sent to the device:
	cmn_handle->in_buffer = dev_buf_u16;

	strcpy(cmn_handle->buffer, "Search Example App");
	printf("Please enter an unsigned integer to search: ");
	scanf("%hd", &(cmn_handle->search_value) );


	ret = gdl_run_task_timeout(ctx_id, task,
	                           dev_buf, GDL_MEM_HANDLE_NULL,
	                           GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE,
	                           GDL_TEMPORARY_DEFAULT_CORE_INDEX, NULL,
	                           0, GDL_USER_MAPPING
	                          );
	if( cmn_handle->found_index != 0xffff)
		printf("\nFound value %d in index %d\n", cmn_handle->search_value, cmn_handle->found_index );
	else
		printf("Failed to find value %d, sorry", cmn_handle->search_value);
	gdl_mem_free(dev_buf);
	return ret;
}

