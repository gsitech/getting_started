/*
 * Copyright (C) 2020, GSI Technology, Inc. All easts reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#include <string.h>
#include <stdio.h>
#include <gsi/libgdl.h>
#include <gsi/libsys.h>
#include <gsi/gsi_sim_config.h>

#include "gvml_app-intern.h"
#include "dv_my_gvml_app-defs.h"// #include "gvml_app-defs.h"

#define NUM_CTXS 1


static struct gsi_sim_contexts g_ctxs[NUM_CTXS] = {
	{
		.apu_count = 1,
		.apucs_per_apu = 4,
		.mem_size = 0x40000000,
	}
};

int main(int argc, char *argv[])
{
	
	int ret = 0;
	unsigned int num_ctxs;
	long mem_size, num_apucs;
	struct gdl_context_desc contexts_desc[GDL_MAX_NUM_CONTEXTS];

	gsi_libsys_init(argv[0], true);

	gsi_sim_create_simulator(NUM_CTXS, g_ctxs);

	gdl_init();
	gdl_context_count_get(&num_ctxs);
	gdl_context_desc_get(contexts_desc, num_ctxs);

	gdl_context_property_get(contexts_desc[0].ctx_id, GDL_CONTEXT_MEM_SIZE, &mem_size);
	gdl_context_property_get(contexts_desc[0].ctx_id, GDL_CONTEXT_NUM_APUCS, &num_apucs);

	printf("Num Contexts = %u\n", num_ctxs);
	printf("Memory Size = %ld\n", mem_size);
	printf("Num Apucs = %ld\n", num_apucs);

	if(argc < 2){
		printf("Please enter test type:\nand, xnor, nand, gt, io OR search");
	}

	ret = gdl_context_alloc(contexts_desc[0].ctx_id, 0, NULL, NULL);
	if (ret) {
		printf("Failed to allocate GDL context (err = %d)!!!\n", ret);

		return ret;
	}
	/* ************************************ BOLEAN OPERATIONS TASKS ************************************ */
	if(argc>=2 && strcmp(argv[1],"and")==0 ) 
		ret = run_gvml_and(contexts_desc[0].ctx_id);
	if(argc>=2 && strcmp(argv[1],"xnor")==0 ) 
		ret |= run_gvml_xnor(contexts_desc[0].ctx_id);
	if(argc>=2 && strcmp(argv[1],"nand")==0 ) 
		ret |= run_gvml_nand(contexts_desc[0].ctx_id);
	/* ************************************ Logical operations TASKS ************************************ */
	if(argc>=2 && strcmp(argv[1],"gt")==0 ) 
		ret |= run_greater_than_with_marker(contexts_desc[0].ctx_id);
	/* ************************************ I/O TASKS ************************************ */
	if(argc>=2 && strcmp(argv[1],"io")==0 ){
		ret |= run_IO_intro_task1(contexts_desc[0].ctx_id);
		ret |= run_IO_intro_task2(contexts_desc[0].ctx_id);
		
	} 
	/* ************************************ Search TASKS ************************************ */
	if(argc <2 || (argc>=2 && strcmp(argv[1],"search")==0 )) 
		ret |= run_search_task(contexts_desc[0].ctx_id);

	gdl_context_free(contexts_desc[0].ctx_id);
	gdl_exit();

	gsi_sim_destroy_simulator();
	gsi_libsys_exit();
	if (ret != 0) {
		printf("\nExample GVML App Failed\n");
	} else {
		printf("\nExample GVML App Passed\n");
	}
	return ret;
}
